import React from 'react'
import styles from '../styles/login.module.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { useUserContext } from '../components/context/UserContext'


import { useState } from 'react'

function Login() {

	const {setUserLoggedIn, setUserData} = useUserContext()

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [errorCred, setErrorCred] = useState(false)
	const nav = useNavigate()

	const localStorage = window.localStorage

	const login = async (e) => {
		e.preventDefault()
		console.log("LOGGING IN")
		const res = await axios.post('https://whispering-anchorage-97427.herokuapp.com/api/users/login', {email : email, password : password})
		
		console.log(res)

		if(res.data.result === 'WC') {
			setErrorCred(true)
		} else {
			console.log("OK!");


			localStorage.setItem('token' , `Bearer ${res.data.result.auth}` )
			setUserLoggedIn(true)
			nav('/')
			window.location.reload()
		}

		
	}

	return (
		<div className={styles.pageContainer}>
			<div className={styles.container}>
				<div className={styles.inputSection}>
					<label htmlFor="email">Email: </label>
					<input  type="text" onChange={(e) => setEmail(e.target.value)} />
				</div>
				<div className={styles.inputSection}>
					<label htmlFor="password">Password: </label>
					<input type="password" onChange={(e) => setPassword(e.target.value)}/>
				</div>

				<div className={styles.buttonSection}>
					<button className={`${styles.button} ${styles.loginButton}`} onClick={login}>Login</button>
					<button className={`${styles.button} ${styles.registerButton}`}>Sign up</button>
				</div>
			
				{errorCred && <p>Wrong Credentials!</p>}
			</div>
		</div>
		
	)
}



export default Login