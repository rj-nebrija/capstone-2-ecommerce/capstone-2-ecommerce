import React from 'react'
import styles from '../styles/categories.module.css'
import {useState, useEffect} from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

function Categories() {

	const [categories, setCategories] = useState([])
	const nav = useNavigate()

	const fetchCategories = async () => {
		const res = await axios.get('https://whispering-anchorage-97427.herokuapp.com/api/products/categories/all')
		
		console.log(res)
		setCategories(res.data.result)
	}
	
	useEffect(()=> {
		fetchCategories()
	}, [])

	return (
		<div className={styles.pageContainer}>
			<div className={styles.container}>
				{categories.map(cat => {return (
					<div className={styles.card} key={cat.name} onClick= {()=> nav(`/categories/${cat.name}`)}>
						<img src={cat.imagePath} alt="" />
						<p>{cat.name}</p>
					</div>
				)})}
			</div>
		</div>
	)
}

export default Categories