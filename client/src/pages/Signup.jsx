import React from 'react'
import styles from '../styles/signup.module.css'
import {useState, useEffect} from 'react'
import axios from 'axios'
import {useNavigate} from 'react-router-dom'



function Signup() {

	const nav = useNavigate()

	const [fName, setFName] = useState('')
	const [lName, setLName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPass, setConfirmPass] = useState('')
	const [existingAcc, setExistingAcc] = useState(false)
	const [notSamePass, setNotSamePass] = useState(false)
	const [error, setError] = useState(false)


	const signUp = async () => {

		//check first if password and confirm password is the same
		if(password !== confirmPass) {
			console.log("NOT SAME PSS");
			setNotSamePass(true)
			return
		} else {
			//redirect to login page
			setNotSamePass(false)
		}

		const res = await axios.post('https://whispering-anchorage-97427.herokuapp.com/api/users/register', 
																			{
																				firstName : fName,
																				lastName : lName,
																				email : email,
																				password : password
																			})

		if(res.data.result == "EA") {
			setExistingAcc(true)
		} else if(res.data.result == "ERROR") {
			setExistingAcc(false)
			setError(true)

		} else if(res.data.result == "OK") {
			setExistingAcc(false)
			setError(false)
			alert('User successfully registered!')
			nav('/login')

		}
	}

	const checkIfSignedIn = async () => {
		const res = await axios.get('https://whispering-anchorage-97427.herokuapp.com/api/users/verifyLoggedIn', {headers : {'Authorization' : localStorage.getItem('token')}})

		if(res.data.auth == "VERIFIED") {
			//if logged in go back to home
			console.log("LOGGED IN")
			nav('/')
		} else {
			//do nothing if not 
			return
		}
	}


	useEffect(()=> {
		checkIfSignedIn()
	}, [])

	return (
		<div className={styles.pageContainer} >
			<div className={styles.container}>
				<div className={styles.inputSection}>
					<label htmlFor="firstName">First Name: </label>
					<input type="text" id='firstName' onChange={(e) => setFName(e.target.value)}/>
				</div>
				<div className={styles.inputSection}>
					<label htmlFor="lastName">Last Name: </label>
					<input type="text" id='lastName'onChange={(e) => setLName(e.target.value)}/>
				</div>
				<div className={styles.inputSection}>
					<label htmlFor="email">Email: </label>
					<input type="text" id='email' onChange={(e) => setEmail(e.target.value)}/>
				</div>
				<div className={styles.inputSection}>
					<label htmlFor="password">Password: </label>
					<input type="password" id='password'onChange={(e) => setPassword(e.target.value)}/>
				</div>
				<div className={styles.inputSection}>
					<label htmlFor="confirmPass">Confirm Password: </label>
					<input type="password" id='confirmPass'onChange={(e) => setConfirmPass(e.target.value)}/>
				</div>

				{existingAcc && <p>Error! Email already exists...</p>}
				{notSamePass && <p>Error! Confirm password must be the same...</p>}


				<div className={styles.buttonSection}>
					<button className={styles.signupButton} onClick={signUp}>Sign up!</button>
				</div>

			</div>
		</div>
	)
}

export default Signup