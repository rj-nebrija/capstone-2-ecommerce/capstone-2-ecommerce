import React from 'react'
import styles from '../styles/home.module.css'

import Featured from '../components/Featured'
import Showcase from '../components/Showcase'

import { useState , useEffect } from 'react'

import axios from 'axios'

function Home() {

	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(true)

	const fetchProducts = async () => {
		const res = await axios.get('https://whispering-anchorage-97427.herokuapp.com/api/products/details/active')

		setProducts(res.data.result)
	}

		useEffect(() => {
			fetchProducts()
			setIsLoading(true)
		}, [])

		console.log(products)

		return (
			<div className={styles.container}>
				<Featured />
				<Showcase products={products} />
			</div>
		)
}

export default Home