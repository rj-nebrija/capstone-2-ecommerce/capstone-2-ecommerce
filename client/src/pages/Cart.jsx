import React from 'react'

import styles from '../styles/cart.module.css'

import {useState , useEffect} from 'react'

import { useUserContext } from '../components/context/UserContext'
import axios from 'axios'

function Cart() {

	const [products, setProducts] = useState([]) 
	const [totalAmount, setTotalAmount] = useState(0)
	const [status, setStatus] = useState(0)

	const getOrders = async () => {
		const res = await axios.get('https://whispering-anchorage-97427.herokuapp.com/api/users/myOrders', {headers : {'Authorization' : localStorage.getItem('token')}})
		console.log(res)

		setProducts([...res.data.result.products])
		setTotalAmount(res.data.result.totalAmount)
		setStatus(res.data.result.deliveryStatus)
	}

	const getStatus = (index) => {
		if(status == index) {
			return styles.inProgress
		} else {
			return 
		}
	}

	const deleteProductFromOrder = async (index) => {
		const res = await axios.delete('https://whispering-anchorage-97427.herokuapp.com/api/users/myOrders/deleteProduct', {headers : {'Authorization' : localStorage.getItem('token')}, data : {index : index }})

		if(res.data.result == "OK") {
			alert("Successfully deleted order from cart!")
			window.location.reload()
		} else {
			alert("ERROR! Something went wrong..")
			window.location.reload()
		}
		console.log(res)
	} 

	const handlePay = async () => {
		const res = await axios.post('https://whispering-anchorage-97427.herokuapp.com/api/users/myOrders/pay' ,{headers : {'Authorization' : localStorage.getItem('token')}, data : {}})

		if(res.data.result == "OK") {
			await getOrders()
		} else {
			alert("ERROR! Something went wrong..")
			window.location.reload()
		}
	}
	useEffect(()=> {
		getOrders()
	}, [])

	return (
		<div className={styles.pageContainer}>
			<div className={styles.container}>
				<div className={`${styles.section} ${styles.tableSection}`}>

					<table className={styles.orderTable}>
								<thead>
									<tr>
										<th>Name</th>
										<th>Price</th>
										<th>Amount</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									{products.map((product,i) => {
										return (
											<tr key={i}>
												<th>{product.productDetails.name}</th>
												<th>{product.productDetails.price}</th>
												<th>{product.amount}</th>
												<th>{product.productDetails.price * product.amount}</th>
												{status < 1 ? <th><button onClick={()=> deleteProductFromOrder(i)}>X</button></th> : <th></th>}	
											</tr>
										)})
									}
									<tr>
										<th></th>
										<th></th>
										<th>Total Amount: </th>
										<th>{totalAmount}</th>
									</tr>
								</tbody>
							</table>
				</div>

				{status == 0 ? 
				
					<div className={`${styles.section} ${styles.buttonSection}`}>
						<button onClick={handlePay}>Pay Now!</button>
					</div>

				:

					<div className={`${styles.section} ${styles.statusSection}`}>
						<div className={`${styles.statusItem} ${getStatus(1)}`}>
							<img src="/images/cart/paid.png" alt="" />
							<span>Payment</span>
						</div>

						<div className={`${styles.statusItem} ${getStatus(2)}`}>
							<img src="/images/cart/bike.png" alt="" />
							<span>On the way</span>
						</div>
							
						<div className={`${styles.statusItem} ${getStatus(3)}`}>
							<img src="/images/cart/delivered.png" alt="" />
							<span>Delivered</span>
						</div>
					</div>
				}				
			</div>
		</div>
	)
}


export default Cart