import React from 'react'
import styles from '../styles/productinfo.module.css'
import { useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { useUserContext } from '../components/context/UserContext'
import sampleImage from '../img/pizza.png'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

function ProductInfo() {
	const {id} = useParams()

	const {userData, userLoggedIn} = useUserContext()

	const [name, setName] = useState('Sample Name')
	const [description, setDescription] = useState('Sample Description')
	const [price, setPrice] = useState(100.50)
	const [rating, setRating] = useState(3.4)
	const [categories, setCategories] = useState([{name : "Sample1"} , {name :"Sample2"}])
	const [imagePath, setImagePath] = useState(sampleImage)
	const [amount, setAmount] = useState(0)
	const [productFound, setProductFound] = useState(false)
	const nav = useNavigate()

	const addToCart = async () => {
		console.log("ADDING PRODUCT ")
		console.log(id)
		console.log(amount)

		//check if logged in first
		if(localStorage.getItem('token') == null) {
			//if no token go to login page
			alert("User not logged in!")
			nav('/login')
			return
		}

		if(amount <= 0) {
			alert("Please enter valid amount!")
			setAmount(0)
			return
		}

		const productInfo = {
			productId : id,
			amount : amount
		}

		const res = await axios.post('https://whispering-anchorage-97427.herokuapp.com/api/users/addToCart', productInfo, {headers : {'Authorization' : localStorage.getItem('token')}})
		
		if(res.data.result == "ERROR") {

		} else {
			console.log(res)
			alert("ADDED TO CART!")
			window.location.reload()
		}
	}



	useEffect(()=> {

		const findProduct = async () => {
			try {
				const res = await axios.get(`https://whispering-anchorage-97427.herokuapp.com/api/products/details/${id}`)
				
				setProductFound(true)
				//setProdssuct( prev =wWss>  ({...prev, ...res.data.result}))

				const result = res.data.result
				console.log(result)
				setName(result.name)
				setPrice(result.price)
				setDescription(result.description)
				setRating(result.rating)
				setCategories(result.categories)
				setImagePath(result.imagePath)

			}catch(err) {
				console.log("ERRPR")
				console.log(err)
			}
		
		}


		findProduct()
		
	}, [])

	console.log(imagePath)
	return (
		<div className={styles.container}>
			<div className={styles.imageSection}>
				<img src={imagePath} alt="" />
				{/* <p>{product.categories.map(p => {return <p>{p.name}</p>})}</p> */}
			</div>

			<div className={styles.infoSection}>
				<h1>{name}</h1>

				<span><h3>{rating} / 5.0</h3></span>

				<h2>{description}</h2>
			
				<span><strong>Categories: </strong> {categories.map( (category, i) => {
					return (<span key={i}>{category.name}, </span>)
				})}</span>
			</div>

			<div className={styles.cartSection}>
				<h3>₱{price}</h3>
				<p>Shipping to Philippines only.</p>
				<p>Delivery : As soon as possible</p>

				<div className={styles.buttonSection}>
					<div>
						<label htmlFor="">Amount:</label>
						<input type="number" onChange={(e)=> {setAmount(e.target.value)}}/>
					</div>
					
					<button onClick={addToCart}>Add to Cart</button>
					<button>Buy Now</button>
					
				</div>
			</div>
		</div>
	)
}

export default ProductInfo