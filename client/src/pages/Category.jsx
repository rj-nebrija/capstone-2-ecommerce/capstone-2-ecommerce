import React from 'react'
import styles from '../styles/category.module.css'
import { useParams } from 'react-router-dom'
import {useState, useEffect} from 'react'
import ItemCard from '../components/ItemCard'
import axios from 'axios'


function Category() {
	const {name} = useParams()

	const [products, setProducts] = useState([])
	

	const fetchProducts = async () => {
		console.log("NAME")
		console.log(name)
		const res = await axios.get(`https://whispering-anchorage-97427.herokuapp.com/api/products/categories/${name}`)

		console.log(res)

		setProducts(res.data.result)

	}

	useEffect(() => {
		fetchProducts()
	}, [])


	return (
		<div className={styles.pageContainer}>
			<h1>{name} Category</h1>
			<div className={styles.container}>
				{products.map(product => {
					return (
						<ItemCard key={product._id} product = {product}/>
					)
				})}
			</div>

		</div>
	)
}

export default Category