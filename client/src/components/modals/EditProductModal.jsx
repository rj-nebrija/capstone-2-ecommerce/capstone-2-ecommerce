import React from 'react'
import styles from '../../styles/editproductmodal.module.css'

import { useState, useEffect } from 'react'
import axios from 'axios'

function EditProductModal({productData, handleEditSubmit}) {
	
	const [isLoading, setIsLoading] = useState(true)
	const [categories, setCategories] = useState([])


	const fetchCategories = async() => {

		const res = await axios.get('https://whispering-anchorage-97427.herokuapp.com/api/products/categories/all')
		console.log("CATEGORIES")
		console.log(res)

		setCategories([...res.data.result])

		setIsLoading(false)
	}

	useEffect(()=> {
		
		fetchCategories()

	}, [])
	
	console.log("CATEGORIES ARRAY");
	console.log(categories)
	return (
		<>
			{!isLoading && 
			
				<div className={styles.background}>
					<div className={styles.modal}>
						<div className={styles.header}>
							<h3>Edit Product</h3>

							<div className={styles.close}>
								<h4>X</h4>
							</div>
						</div>

						<div className={styles.container}>
							<div className={styles.inputGroup}>
								<label htmlFor="name">Name: </label>
								<input type="text" id='name' defaultValue="Product name"/>
							</div>
							<div className={styles.inputGroup}>
								<label htmlFor="description">Description: </label>
								<textarea  id='description' rows={5} cols={50} defaultValue="Product name"/>
							</div>
							<div className={styles.inputGroup}>
								<label htmlFor="price">Price: </label>
								<input type="number" id='price' defaultValue={100}/>
							</div>
							<div className={styles.inputGroup}>
								<label htmlFor="rating">Rating: </label>
								<input type="number" id='rating' defaultValue={100}/>
							</div>

							<button>Edit</button>
						</div>
					</div>
				</div>
			}
			
		</>
		
	)
}

export default EditProductModal