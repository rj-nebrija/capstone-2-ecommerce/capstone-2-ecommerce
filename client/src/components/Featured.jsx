import React from 'react'
import { useState } from 'react'

import arrowLeft from '../img/arrowl.png'
import arrowRight from '../img/arrowr.png'
import styles from '../styles/featured.module.css'
import featured3 from  '../img/featuredimg1.jpg'
import featured2 from  '../img/featuredimg2.png'



function Featured() {

	const [index, setIndex] = useState(0)
	const featured1 = '/images/ecommerce.png'
	const featured4 = '/images/hyperx.jpg'
	const featured5 = '/images/lenovo.jpg'

	//MOCK data for featured
	let featured = [
		featured1,
		featured5, 
		featured2,
		featured3,
		featured4
	]


	const handleArrowClick = (direction) => {
		if(direction === 'l') {
			if(index == 0) {
				setIndex(featured.length - 1)
			} else {
				setIndex(prev => prev - 1)
			}
		} else if (direction === 'r') {
				if(index == featured.length -1) {
					setIndex(0)
				} else {
					setIndex(prev => prev + 1)
				}
		}
	}


	return (
		<div className={styles.container}>
			<div className={styles.arrowContainer} style={{left : 0}} onClick={() => handleArrowClick('l')}>
				<img src={arrowLeft} alt="" />
			</div>

			<div className={styles.wrapper} style={{transform : `translateX(${-100*index}vw)`}}>
				{featured.map((feature, i) => {
					return (
						<div className={styles.imgContainer} key={i} >
							<img src={feature} alt="" />
						</div>
					)}
				)}
			</div>

			<div className={styles.arrowContainer} style={{right : 0}} onClick={ () => handleArrowClick('r')}>
				<img src={arrowRight} alt="" />
			</div>

		</div>
	)
}

export default Featured


