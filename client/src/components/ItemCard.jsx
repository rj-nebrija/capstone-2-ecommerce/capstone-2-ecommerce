import React from 'react'
import styles from '../styles/itemcard.module.css'
import sampleImg from '../img/pizza.png'

import { useState } from 'react'

import { useNavigate } from 'react-router-dom'
import { useUserContext } from './context/UserContext'
import { useEffect } from 'react'

function ItemCard({product}) {

	const [name, setName] = useState(product.name)
	const [description, setDescription] = useState(product.description)
	const [rating, setRating] = useState(product.rating)
	const [price, setPrice] = useState(product.price)
	const [imgPath, setImgPath] = useState(product.imagePath)
	const navigate = useNavigate()
	const {userData} = useUserContext()


	useEffect(() => {
		console.log(userData)
	}, [])


	return (
		<div className={styles.cardContainer} onClick={() => {navigate(`/product/${product._id}`)}}>
			<div className={styles.imgSection}>
				<img src= {imgPath} alt="" />
			</div>

			<div className={styles.titleSection}>
				<h2>{name}</h2>
			</div>

			<div className={styles.priceSection}>
				<p>₱{price}</p>
			</div>
			<div className={styles.ratingSection}>
				<p>{rating.toFixed(1)} / 5.0 </p>
			</div>
			{userData.isAdmin &&
				<div className={styles.editSection}>
					<button>Edit</button>
					<button>Delete</button>
				</div>
			}
		</div>
	)
}

export default ItemCard