import React from 'react'
import styles from  '../styles/navbar.module.css'
import {Outlet, useNavigate} from 'react-router-dom'
import { useUserContext } from './context/UserContext'

function Navbar() {

	const nav = useNavigate()
	const {userLoggedIn, userData, setUserData} = useUserContext()

	const logout = () => {
		localStorage.removeItem('token')
		setUserData(null)
		nav('/')
		window.location.reload()

	}

	return (
		<>
			<div className={styles.container} >

				<div className={styles.item} onClick={() => nav('/categories')}>
					<img src="/images/burger_menu5png.png" alt="" />
				</div>
				
				<div className={`${styles.item} ${styles.title}`} onClick ={() => nav('/')}>
					<h2>Amazonia</h2>
				</div>

				<div className={styles.item}>
					<input type="search" className='search-bar'/>
				</div>
				
				{userLoggedIn && 
					<div className={styles.item}>
						<h3>{`Hello, ${userData.firstName}!`}</h3>
					</div>
				}

				<div className={styles.item}>

					{!userLoggedIn ? 
						
								<div>
									<button className= {styles.loginButton} onClick={()=> nav('/login')} >Login</button> 
									<button className={styles.signupButton} onClick={()=> nav('/signup')}>Sign up</button>
								</div>
							: 
								<div>
									{userData.isAdmin ? <button className={styles.adminButton}>Admin</button> : <button className={styles.cartButton} onClick={() => nav('/cart')}>Cart</button> }
									
									<button className={styles.logoutButton} onClick={logout} >Log out</button>
								</div>
					}
				</div>
			</div>
			<Outlet />
		</>
		

		
	)
}

export default Navbar