import React from 'react'
import styles from '../styles/showcase.module.css'
import { useEffect, useState } from 'react'
import axios from 'axios'

import ItemCard from './ItemCard'
import EditProductModal from './modals/EditProductModal'

function Showcase({products}) {
	const [prod, setProducts] = useState(products) 

	useEffect(()=> {
		setProducts(products)
	}, [products])

	return (
		
		<div className={styles.container}>
			{/* <EditProductModal/> */}
			{prod.map(product => {
				console.log(product)
				return <ItemCard key={product._id} product = {product} />
			})}
		</div>
	)
}

export default Showcase