import React from "react";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import ProductInfo from "./pages/ProductInfo";
import ErrorPage from "./pages/ErrorPage";
import Footer from "./components/Footer";
import Login from "./pages/Login";
import Cart from "./pages/Cart";
import Categories from "./pages/Categories";
import Signup from "./pages/Signup";

import { UserProvider } from "./components/context/UserContext";
import { useState, useEffect } from "react";

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import { useContext } from "react";
import './App.css'
import axios from "axios";
import Category from "./pages/Category";



function App() {

	const [userLoggedIn, setUserLoggedIn] = useState(false)
	const [userData, setUserData] = useState({name : "test"})
	const [isLoading, setIsLoading] = useState(true)

	const verifyUserTokenAndGetDetails = async () => {
		const res = await axios.get('https://whispering-anchorage-97427.herokuapp.com/api/users/details', {headers : {'Authorization' : localStorage.getItem('token')}})

		if(res.data.auth != "failed" && res.data.result != "ERROR") {
			console.log("SETTING USER DATA")
			console.log(res.data.result)
			setUserData({...res.data.result})
			setUserLoggedIn(true)

		} else {
			setUserLoggedIn(false)
		}
		setIsLoading(false)
	}


	useEffect(()=> {
		if(localStorage.getItem('token') == null) {
			console.log("NO TOKEN")
			setUserLoggedIn(false)
			setIsLoading(false)
		} else{
			verifyUserTokenAndGetDetails()
		}
	},[userLoggedIn]) 

  return (
		<>
		{isLoading ?
			<div><h1>Getting user data...</h1></div> 
			
			:

			<UserProvider data = {{userData, userLoggedIn, setUserLoggedIn, setUserData}}>
				<Router>
					<Routes>
							<Route path='/' element={<Navbar/>}>	
								<Route index element={<Home/>}/>
								<Route path='product/:id' element= {<ProductInfo/>} />
								<Route path='login' element={<Login/>}  />
								<Route path='cart' element={<Cart/>} />
								<Route path='signup' element={<Signup/>} />
								<Route path='categories' element={<Categories/>} />
								<Route path='categories/:name' element={<Category/>} />
								<Route path="*" element={<ErrorPage/>} />
							</Route>
					</Routes>
				
			</Router>
			</UserProvider>

		
		}
			
			
		{/* <Footer /> */}
		</>
  );
}

export default App;
